package agendaoo.controle;

import agendaoo.modelo.Pessoa;
import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// metodos
   
    public void adicionar(Pessoa umaPessoa) {
	listaPessoas.add(umaPessoa);
    }

    public void remover(Pessoa umaPessoa) {
        listaPessoas.remove(umaPessoa);
    }
    
    public Pessoa pesquisaNome(String umNome) {
        for (Pessoa umaPessoa: listaPessoas) {
            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) 
                return umaPessoa;
        }
        return null;
    }

    public Pessoa pesquisaTelefone(String umTelefone) {
        for (Pessoa umInforme : listaPessoas) {
            if (umInforme.getTelefone().equals(umTelefone)) 
                return umInforme;
        }
        return null;
    }

    public void exibirContato(String nome, String telefone, String email, String hangout, String endereco, String idade, String sexo) {
        System.out.println("Nome: " +nome +"\n");
        System.out.println("Telefone: " +telefone "\n");
        System.out.println("Email: " +email "\n");
        System.out.println("Hangout: " +hangout "\n");
        System.out.println("Endereço: " +endereco "\n");
        System.out.println("Idade: " + "\n");
        System.out.println("Sexo " +sexo "\n");
        
    }
        
        
        
        

        //exibir Lista
        //for (Pessoa umaPessoa : listaPessoas) {
        //System.out.println(umaPessoa.getNome());
        
    

}
