
package agendaoo.modelo;

public class Pessoa{

//Atributos
	private String nome;
	private String idade;
	private String telefone;
	private char sexo;
	private String email;
	private String hangout;
	private String endereco;

//Construtor
	public Pessoa(){
	}

	public Pessoa(String umNome){
		nome = umNome;
	}

	public Pessoa(String umNome, String umTelefone, String umEmail, String umHangout, String umaIdade, String umEndereco){
		nome = umNome;
		telefone = umTelefone;
                email = umEmail;
                hangout = umHangout;
                idade = umaIdade;
                endereco = umEndereco;
	}
	

//MÃ©todos

	public void setNome(String umNome){
		nome = umNome;
	}

	public String getNome(){
		return nome;
	}

	public void setIdade(String umaIdade){
		idade = umaIdade;
	}

	public String getIdade(){
		return idade;
	}

	public void setTelefone(String umTelefone){
		telefone = umTelefone;
	}

	public String getTelefone(){
		return telefone;
	}

	public void setSexo(char umSexo){
		sexo = umSexo;
	}

	public char getSexo(){
		return sexo;
	}

	public void setEmail(String umEmail){
		email = umEmail;
	}

	public String getEmail(){
		return email;
	}

	public void setHangout(String umHangout){
		hangout = umHangout;
	}

	public String getHangout(){
		return hangout;
	}

	public void setEndereco(String umEndereco){
		endereco = umEndereco;
	}

	public String getEndereco(){
		return endereco;
	}

	
}

